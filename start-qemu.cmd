setlocal
set QEMU_PATH=C:\app\qemu
:: %QEMU_PATH%\qemu-system-arm -M versatilepb -m 128M -serial vc:80Cx24C -kernel test.bin
:: %QEMU_PATH%\qemu-system-arm -M versatilepb -m 128M -serial vc:80Cx24C -kernel test.bin
:: %QEMU_PATH%\qemu-system-arm -M versatilepb -m 128M -serial COM4 -kernel test.bin
:: %QEMU_PATH%\qemu-system-arm -M versatilepb -m 128M -nographic -serial COM4 -kernel test.bin
@if not "%*" == "" (
  @%QEMU_PATH%\qemu-system-arm --help
  @echo help
) else (
  %QEMU_PATH%\qemu-system-arm -M versatilepb -m 128M -serial vc:80Cx24C -s -kernel test.bin
)

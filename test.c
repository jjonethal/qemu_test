
volatile unsigned int * const UART0DR = (unsigned int *)0x101f1000;
 
void print_uart0(const char *s) {
 while(*s != '\0') { /* Loop until end of string */
 *UART0DR = (unsigned int)(*s); /* Transmit char */
 s++; /* Next char */
 }
}

int puts(const char *s) {
  print_uart0(s);
}

int put_char(int c) {
  *UART0DR = c;
  return 0;
}

 
void c_entry() {
  int i=0;
  while(1) {
    print_uart0("Hello world!\r\n");
  }
}

